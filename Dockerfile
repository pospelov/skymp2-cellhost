FROM gcc:8.2
RUN  apt-get update -y && \
     apt-get upgrade -y && \
     apt-get dist-upgrade -y && \
     apt-get -y autoremove && \
     apt-get clean
RUN apt-get install -y p7zip \
    p7zip-full \
    unace \
    zip \
    unzip \
    xz-utils \
    sharutils \
    uudeview \
    mpack \
    arj \
    cabextract \
    file-roller \
    && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/Microsoft/vcpkg.git && cd vcpkg && ./bootstrap-vcpkg.sh
RUN ./vcpkg/vcpkg install boost-system boost-filesystem boost-iostreams boost-asio boost-array boost-lockfree
RUN wget https://cmake.org/files/v3.12/cmake-3.12.1-Linux-x86_64.sh && chmod 777 ./cmake-3.12.1-Linux-x86_64.sh && ./cmake-3.12.1-Linux-x86_64.sh --skip-license

COPY . /usr/src/skymp2
RUN mkdir -p /skymp2-build && \
    cd /skymp2-build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SERVER=TRUE /usr/src/skymp2 && \
    cmake --build .
CMD /skymp2-build/skymp2-cellhost-exe
