#pragma once
#include <unordered_map>
#include <set>
#include <vector>

#include "IStreamer.h"
#include "DSLine.h"

class Grid : public IStreamer {

public:
	void moveTo(formid key, const Point &dest) noexcept override {
		auto it = m_moveByKey.find(key);
		if (it != m_moveByKey.end()) {
			auto &moveByKey = it->second;
			m_keysByMove[moveByKey[0]][moveByKey[1]][moveByKey[2]].erase(key);
		}
		auto &moveByKey = m_moveByKey[key];
		moveByKey = dest;
		m_keysByMove[dest[0]][dest[1]][dest[2]].insert(key);
	}

	void erase(formid key) noexcept override {
		auto moveByKey = m_moveByKey[key];
		if (m_moveByKey.erase(key)) {
			m_keysByMove[moveByKey[0]][moveByKey[1]][moveByKey[2]].erase(key);
		}
	}
	void forEachNeighbour(formid key, std::function<void(formid)> f) const noexcept override {
		Point moveByKey;
		try {
			moveByKey = m_moveByKey.at(key);
		}
		catch (...) {
			return;
		}
		for (int x = -1; x <= 1; ++x) {
			for (int y = -1; y <= 1; ++y) {
				auto &neighbours = m_keysByMove[x + moveByKey[0]][y + moveByKey[1]][moveByKey[2]];
				for (auto nei : neighbours) {
					f(nei);
				}
			}
		}
	}

private:
	std::unordered_map<formid, Point> m_moveByKey;
	mutable DSLine<DSLine<std::unordered_map<formid, std::set<formid>>>> m_keysByMove;
};