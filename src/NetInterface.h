#pragma once
#include <vector>

using UserID = unsigned short;
constexpr UserID g_invalidUserId = ~0;

using RawMessage = std::vector<unsigned char>;

struct NetEvent {
	enum Type {
		INVALID = 0, // Used for empty or invalid Event structures
		CONNECT = 1, // A new UserID was allocated / You were connected to the server
		DISCONNECT = 2, // UserID will be recycled / You were disconnected from the server
		MESSAGE = 3,
		CONNECTION_ATTEMPT_FAILED = 4
	};

	Type type = INVALID;
	RawMessage msg; // empty if type != MESSAGE
};

enum class Reliability {
	Unreliable, Reliable
};

class IServer {
public:
	virtual void open(unsigned short port) = 0; // May throw std::runtime_error with implementation defined message
	virtual bool receiveFrom(UserID &userId, NetEvent &event) noexcept = 0;
	virtual void sendTo(UserID target, const void *message, size_t size, Reliability reliability) noexcept = 0;

	virtual ~IServer() = default;
};

class IClient {
public:
	virtual void connect(const char *ipAddress, unsigned short port) = 0; // May throw std::runtime_error with implementation defined message
	virtual bool receive(NetEvent &event) noexcept = 0;
	virtual void send(const RawMessage &message, Reliability reliability) noexcept = 0;
	virtual int getAvgPing() const noexcept = 0;

	virtual ~IClient() = default;
};