#include <memory>
#include <list>
#include <map>
#include <array>

#include <RakPeer.h>
#include <MessageIdentifiers.h>

#include "Rak.h"

enum : unsigned char {
	ID_MESSAGE = ID_USER_PACKET_ENUM
};

constexpr int g_maxConnections = 2000;

void sendToImpl(const std::unique_ptr<RakNet::RakPeerInterface> &peer, RakNet::RakNetGUID guid, const void *message, size_t size, Reliability reliability) {
	RakNet::BitStream bs;
	bs.Write(ID_MESSAGE);
	bs.Write((const char *)message, size);
	auto rakReliability = PacketReliability::UNRELIABLE;
	if (reliability == Reliability::Reliable) {
		rakReliability = PacketReliability::RELIABLE;
	}
	peer->Send(&bs, PacketPriority::MEDIUM_PRIORITY, rakReliability, 0, guid, false);
}

void getMessageFromPacket(const RakNet::Packet &packet, RawMessage &msg) {
	if (packet.length > 1) {
		msg.resize(packet.length - 1);
		memcpy(&msg[0], packet.data + 1, packet.length - 1);
	}
}

class IdManager
{
public:
	IdManager() {
		for (size_t i = 0; i < std::size(guidById); ++i) {
			guidById[i] = RakNet::UNASSIGNED_CRABNET_GUID;
		}
	}

	UserID allocateId(const RakNet::RakNetGUID &guid) {
		const UserID res = nextId++;
		while (guidById[nextId] != RakNet::UNASSIGNED_CRABNET_GUID) {
			++nextId;
			assert(nextId != g_maxConnections);
		}
		guidById[res] = guid;
		idByGuid[guid] = res;
		return res;
	}

	void freeId(UserID id) {
		const auto guid = guidById[id];
		guidById[id] = RakNet::UNASSIGNED_CRABNET_GUID;
		idByGuid.erase(guid);
		if (id < nextId) nextId = id;
	}

	UserID find(const RakNet::RakNetGUID &guid) {
		try {
			return idByGuid.at(guid);
		}
		catch (...) {
			return g_invalidUserId;
		}
	}

	RakNet::RakNetGUID find(UserID id) {
		if (id >= g_maxConnections || (long long)id < 0 /* what if we change UserID type to signed and pass -1? */) {
			return RakNet::UNASSIGNED_CRABNET_GUID;
		}
		return guidById[id];
	}

private:
	std::map<RakNet::RakNetGUID, UserID> idByGuid;
	std::array<RakNet::RakNetGUID, g_maxConnections> guidById;
	UserID nextId = 0;
};

struct RakServer::Impl {
	std::unique_ptr<RakNet::RakPeerInterface> peer;
	std::unique_ptr<RakNet::SocketDescriptor> socket;
	std::unique_ptr<IdManager> idManager;

	void open(unsigned short port) {
		idManager.reset(new IdManager);
		peer.reset(new RakNet::RakPeer);
		socket.reset(new RakNet::SocketDescriptor(port, nullptr));
		const auto res = peer->Startup(g_maxConnections, &*socket, 1);
		if (res != RakNet::StartupResult::CRABNET_STARTED) {
			throw std::runtime_error("peer startup failed with code " + std::to_string((int)res));
		}
		peer->SetMaximumIncomingConnections(g_maxConnections);
	}

	/*void tick() {
		assert(peer); // Did you forget to call open?
		RakNet::Packet *packet;
		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive()) {
			handle(*packet);
		}
	}*/

	void handle(const RakNet::Packet &packet, UserID &userId, NetEvent &netEvent) {
		netEvent.msg.clear();
		userId = g_invalidUserId;

		switch (packet.data[0]) {
		case ID_DISCONNECTION_NOTIFICATION:
		case ID_CONNECTION_LOST:
			userId = idManager->find(packet.guid);
			if (userId != g_invalidUserId) {
				idManager->freeId(userId);
			}
			netEvent.type = NetEvent::DISCONNECT;
			break;
		case ID_NEW_INCOMING_CONNECTION:
			userId = idManager->allocateId(packet.guid);
			netEvent.type = NetEvent::CONNECT;
			break;
		case ID_MESSAGE:
			userId = idManager->find(packet.guid);
			netEvent.type = NetEvent::MESSAGE;
			getMessageFromPacket(packet, netEvent.msg);
			break;
		default:
			return;
		}
	}

	bool receiveFrom(UserID &userId, NetEvent &event) {
		/*if (events.empty()) return false;
		const auto &pair = events.front();
		userId = pair.first;
		event = pair.second;
		events.pop_front();*/

		auto packet = peer->Receive();
		if (!packet) return false;
		handle(*packet, userId, event);
		peer->DeallocatePacket(packet);
		return true;
	}

	void sendTo(UserID target, const void *message, size_t size, Reliability reliability) {
		const auto guid = idManager->find(target);
		assert(guid != RakNet::UNASSIGNED_CRABNET_GUID);
		sendToImpl(peer, guid, message, size, reliability);
	}
};

RakServer::RakServer() : pImpl (new Impl) {
}

RakServer::~RakServer() {
	delete pImpl;
}

void RakServer::open(unsigned short port) {
	pImpl->open(port);
}
bool RakServer::receiveFrom(UserID &userId, NetEvent &event) noexcept {
	return pImpl->receiveFrom(userId, event);
}

void RakServer::sendTo(UserID target, const void *message, size_t size, Reliability reliability) noexcept {
	pImpl->sendTo(target, message, size, reliability);
}

struct RakClient::Impl {
	std::unique_ptr<RakNet::RakPeerInterface> peer;
	std::unique_ptr<RakNet::SocketDescriptor> socket;
	RakNet::RakNetGUID serverGuid;
	std::list<NetEvent> events;

	void connect(const char *ipAddress, unsigned short port) {
		serverGuid = RakNet::UNASSIGNED_CRABNET_GUID;
		peer.reset(new RakNet::RakPeer);
		socket.reset(new RakNet::SocketDescriptor(0, nullptr));
		const auto res = peer->Startup(1, &*socket, 1);
		if (res != RakNet::StartupResult::CRABNET_STARTED) {
			throw std::runtime_error("peer startup failed with code " + std::to_string((int)res));
		}
		const auto conRes = peer->Connect(ipAddress, port, "", 0);
		if (conRes != RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED) {
			throw std::runtime_error("peer connect failed with code " + std::to_string((int)res));
		}
	}

	void tick() {
		RakNet::Packet *packet;
		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive()) {
			handle(*packet);
		}
	}

	void handle(const RakNet::Packet &packet) {
		if (serverGuid == RakNet::UNASSIGNED_CRABNET_GUID) {
			serverGuid = packet.guid;
		}

		NetEvent netEvent;

		switch (packet.data[0]) {
		case ID_MESSAGE:
			netEvent.type = NetEvent::MESSAGE;
			getMessageFromPacket(packet, netEvent.msg);
			break;
		case ID_ALREADY_CONNECTED:
		case ID_CONNECTION_ATTEMPT_FAILED:
		case ID_CONNECTION_BANNED:
		case ID_INVALID_PASSWORD:
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
		case ID_IP_RECENTLY_CONNECTED:
			netEvent.type = NetEvent::CONNECTION_ATTEMPT_FAILED;
			break;
		case ID_CONNECTION_LOST:
		case ID_DISCONNECTION_NOTIFICATION:
			netEvent.type = NetEvent::DISCONNECT;
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			netEvent.type = NetEvent::CONNECT;
			break;
		default:
			return;
		}

		events.push_back(netEvent);
	}

	bool receive(NetEvent &event) {
		if (!events.empty()) {
			event = events.front();
			events.pop_front();
			return true;
		}
		return false;
	}
};

RakClient::RakClient() : pImpl(new Impl) {
}

RakClient::~RakClient() {
	delete pImpl;
}

void RakClient::connect(const char *ipAddress, unsigned short port) {
	pImpl->connect(ipAddress, port);
}

bool RakClient::receive(NetEvent &event) noexcept {
	pImpl->tick();
	return pImpl->receive(event);
}

void RakClient::send(const RawMessage &message, Reliability reliability) noexcept {
	sendToImpl(pImpl->peer, pImpl->serverGuid, message.data(), message.size(), reliability);
}

int RakClient::getAvgPing() const noexcept {
	return pImpl->peer->GetAveragePing(pImpl->serverGuid);
}