#include <iostream>
#include <chrono>
#include <thread>
#include <ctime>

#include "Rak.h"
#include "Cellhost.h"
#include "Cluster.h"
#include "Grid.h"

int main() {
	std::cout << "Hello world" << std::endl;

	Grid grid;
	RakServer server;
	server.open(7000);

	Cellhost cellhost(&server, &grid);
	while (1) {
		auto was = clock();
		cellhost.tick();
		///std::cout << "ticked in " << float(clock() - was) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	return 0;
}