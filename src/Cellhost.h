#pragma once
#include <string>

#include "NetInterface.h"
#include "IStreamer.h"
#include "AutorecClient.h"

struct CHMessagePrefix {
	int16_t cellX = 0;
	int16_t cellY = 0;
	IStreamer::formid cellOrWorld = 0;
	IStreamer::formid ownerId = 0;
	IStreamer::formid myId = 0;
};

class Cellhost {
public:
	Cellhost(IServer *server, IStreamer *streamer);
	~Cellhost();

	void tick() noexcept;

private:
	struct Impl;
	Impl *const pImpl;

	Cellhost(const Cellhost &) = delete;
	Cellhost &operator=(const Cellhost &) = delete;
};

class CellhostClient {
public:
	CellhostClient(IClient *client); // must be autoreconnecting
	~CellhostClient();

	void send(const CHMessagePrefix &prefix, const std::string &body) noexcept;
	std::string receive() noexcept; // returns empty string on failure

	const IClient *getClient() const noexcept;

private:
	struct Impl;
	Impl *const pImpl;

	CellhostClient(const CellhostClient &) = delete;
	CellhostClient &operator=(const CellhostClient &) = delete;
};