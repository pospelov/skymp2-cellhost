#include <cassert>
#include <cstdint>
#include <type_traits>
#include <cstring>
#include <list>
#include <ctime>
#include <iostream>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

#ifdef _WIN32
#include <WinSock2.h>
#else
#include <arpa/inet.h>
#endif

#include "Cellhost.h"

using formid = IStreamer::formid;

constexpr auto g_prefixSize = sizeof(CHMessagePrefix::cellX) 
+ sizeof(CHMessagePrefix::cellY) 
+ sizeof(CHMessagePrefix::cellOrWorld) 
+ sizeof(CHMessagePrefix::ownerId) 
+ sizeof(CHMessagePrefix::myId);

constexpr clock_t g_hostTimeout = CLOCKS_PER_SEC * 2;

std::string createPrefix(CHMessagePrefix p) {
	char buf[g_prefixSize + 1];
	buf[g_prefixSize] = '\000';

	char *cursor = buf;

	auto write = [&](auto &argument, auto hton) {
		argument = hton(argument);
		memcpy(cursor, &argument, sizeof(argument));
		cursor += sizeof(argument);
	};

	write(p.cellX, htons);
	write(p.cellY, htons);
	write(p.cellOrWorld, htonl);
	write(p.ownerId, htonl);
	write(p.myId, htonl);

	std::string res;
	res.reserve(g_prefixSize);
	for (size_t i = 0; i != g_prefixSize; ++i) res.push_back(buf[i]);
	return res;
}

CHMessagePrefix decodePrefix(const void *data) {
	CHMessagePrefix p;

	auto cursor = (uint8_t *)data;

	auto read = [&](auto &argument, auto ntoh) {
		using T = typename std::remove_reference<decltype(argument)>::type;
		argument = *(T *)cursor;
		argument = ntoh(argument);
		cursor += sizeof(argument);
	};

	read(p.cellX, ntohs);
	read(p.cellY, ntohs);
	read(p.cellOrWorld, ntohl);
	read(p.ownerId, ntohl);
	read(p.myId, ntohl);

	return p;
}

struct Cellhost::Impl {
	IServer *const server;
	IStreamer *const streamer;

	std::unordered_map<formid, UserID> hosts;
	std::unordered_map<formid, clock_t> lastItemMovUpd;
	NetEvent tempNe;

	UserID getHost(formid refId) {
		try {
			return hosts.at(refId);
		}
		catch (...) {
			return g_invalidUserId;
		}
	}

	void setHost(formid refId, UserID host) {
		if (host == g_invalidUserId) {
			hosts.erase(refId);
			return;
		}
		hosts[refId] = host;
	}

	void tick() {
		UserID userId;
		while (server->receiveFrom(userId, tempNe)) {
			switch (tempNe.type) {
			case NetEvent::CONNECT:
				onConnect(userId);
				break;
			case NetEvent::DISCONNECT:
				onDisconnect(userId);
				break;
			case NetEvent::MESSAGE:
				auto prefix = decodePrefix(tempNe.msg.data());
				auto message = std::string({ tempNe.msg.begin() + g_prefixSize, tempNe.msg.end() });
				try {
					onMessage(userId, prefix, message);
				}
				catch (std::exception &e) {
					std::cerr << "\n" << e.what() << "\nMessage: " << message << std::endl;
				}
				break;
			}
		}
	}

	void onConnect(UserID id) {
		std::cout << "Connected " << id << std::endl;
	}

	void onDisconnect(UserID id) {
		std::cout << "Disconnected " << id << std::endl;
		streamer->erase(id);
	}

	void onMessage(UserID userId, const CHMessagePrefix &p, const std::string &message) {
		if (p.cellX != -10000 && p.cellY != -10000 && p.cellOrWorld) {
			streamer->moveTo(userId, { p.cellX, p.cellY, (int)p.cellOrWorld });
		}

		/*json j;
		try {
			j = json::parse(message);
		}
		catch (std::exception &e) {
			printf("onMessage: %s", e.what()); // TODO: better handling
			return;
		}
		j.erase("token");
		j.erase("host");*/

		const formid ownerId = p.ownerId;

		UserID host = getHost(ownerId);
		if (host == g_invalidUserId) {
			host = userId;
			setHost(ownerId, host);
		}
		if (ownerId < 0xff000000 && host != userId) {
			auto &lastUpd = lastItemMovUpd[ownerId];
			if (lastUpd && clock() - lastUpd > g_hostTimeout) {
				lastUpd = 0;
				setHost(ownerId, g_invalidUserId);
			}
			return;
		}

		if (ownerId < 0xff000000) lastItemMovUpd[ownerId] = clock();

		const bool isMe = p.myId == p.ownerId;
		if (isMe) {
			const auto data = message.data();
			const auto size = message.size();
			streamer->forEachNeighbour(userId, [&](UserID nei) {
				server->sendTo(nei, data, size, Reliability::Unreliable);
			});
		}
		else {
			auto j = json::parse(message);
			j["host"] = "you";
			std::string youHost = j.dump() + '\000';
			j["host"] = "remote";
			std::string remoteHost = j.dump() + '\000';

			const std::vector<unsigned char> youHostV = { youHost.begin(), youHost.end() };
			const std::vector<unsigned char> remoteHostV = { remoteHost.begin(), remoteHost.end() };

			streamer->forEachNeighbour(userId, [&](UserID nei) {
				auto  &message = host == nei ? youHostV : remoteHostV;
				const auto was = clock();
				server->sendTo(nei, message.data(), message.size(), Reliability::Unreliable);
			});
		}
	}
};

Cellhost::Cellhost(IServer *server, IStreamer *streamer) : pImpl(new Impl{ server, streamer }) {
	assert(server);
}

Cellhost::~Cellhost() {
	delete pImpl;
}

void Cellhost::tick() noexcept {
	pImpl->tick();
}

struct CellhostClient::Impl {
	IClient *const client;
	std::list<RawMessage> messages;

	void send(const CHMessagePrefix &prefix, const std::string &body) {
		const std::string msg = createPrefix(prefix) + body;
		client->send({ msg.begin(), msg.end() }, Reliability::Unreliable);
	}

	std::string receive() {
		tick();
		if (messages.empty()) return "";
		const auto val = messages.front();
		messages.pop_front();
		return { val.begin(), val.end() };
	}

	void tick() {
		NetEvent ne;
		while (client->receive(ne)) {
			if (ne.msg.size()) messages.push_back(ne.msg);
		}
	}
};

CellhostClient::CellhostClient(IClient *client) : pImpl(new Impl{ client }) {

}

CellhostClient::~CellhostClient() {
	delete pImpl;
}

void CellhostClient::send(const CHMessagePrefix &prefix, const std::string &body) noexcept {
	pImpl->send(prefix, body);
}

std::string CellhostClient::receive() noexcept {
	return pImpl->receive();
}

const IClient *CellhostClient::getClient() const noexcept {
	return pImpl->client;
}