#include <iostream>
#include <chrono>
#include <thread>
#include <vector>

#include "Rak.h"
#include "Cellhost.h"
#include "Cluster.h"
#include "Grid.h"
#include "AutorecClient.h"

void test() {

	Grid grid;

	RakServer node1;
	node1.open(6998);

	//Cluster server({ &node1 });
	//server.open();
	auto &server = node1;


	Cellhost cellhost(&server, &grid);

	std::vector<CellhostClient *> clients;

	std::cout << "Preparing clients" << std::endl;

	int sz = 0;
	for (int i = 0; i != 1000; ++i) {
		auto autorec = new AutorecClient([] { return new RakClient; });
		autorec->connect("127.0.0.1", 6998);
		clients.push_back(new CellhostClient(autorec));
		for(int j = 0; j < sz; ++j) {
			std::cout << '\b';
		}
		std::cout << i;
		sz = std::to_string(i).size();
	}
	std::cout << std::endl;

	CHMessagePrefix prefix = { -10, 10, 0x3c, 0xff000000, 0xff000000 };

	long long sum = 0, n = 0;

	while (1) {
		clock_t c = clock();
		cellhost.tick();
		auto v = clock() - c;
		if (v != 0) {
			std::cout << v / (double)CLOCKS_PER_SEC << ' ';
			++n;
			sum += v;
			std::cout << sum / n / (double)CLOCKS_PER_SEC << std::endl;
			//goto finish;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		for (int i = 0; i != std::size(clients); ++i) {
			while (clients[i]->receive().size());
			prefix.cellX = rand() % 1;
			prefix.cellY = rand() % 1;
			clients[i]->send(prefix, "{}");
		}
	}
finish:
	while (1) std::this_thread::sleep_for(std::chrono::hours(10000));
}

int main() {
	try {
		test();
		return 0;
	}
	catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		return -1;
	}
}