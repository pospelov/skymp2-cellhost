#pragma once
#include "NetInterface.h"

class RakServer : public IServer {
public:
	void open(unsigned short port) override;
	bool receiveFrom(UserID &userId, NetEvent &event) noexcept override;
	void sendTo(UserID target, const void *message, size_t size, Reliability reliability) noexcept override;

	RakServer();
	~RakServer() override;
private:
	struct Impl;
	Impl *const pImpl;

	RakServer(const RakServer &) = delete;
	RakServer &operator=(const RakServer &) = delete;
};

class RakClient : public IClient {
public:
	void connect(const char *ipAddress, unsigned short port) override;
	bool receive(NetEvent &event) noexcept override;
	void send(const RawMessage &message, Reliability reliability) noexcept override;
	int getAvgPing() const noexcept override;

	RakClient();
	~RakClient() override;
private:
	struct Impl;
	Impl *const pImpl;

	RakClient(const RakClient &) = delete;
	RakClient &operator=(const RakClient &) = delete;
};