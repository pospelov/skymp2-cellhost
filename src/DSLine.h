// https://gitlab.com/pospelov/skymp2-server/
// Contributors: Leonid Pospelov (pospelovlm@yandex.ru), Ivan Savelo (ivaan2002@mail.ru)

#pragma once

template <class T>
class DSLine
{
public:
	T &operator[](long long index) {
		const size_t realIdx = size_t(index >= 0 ? index : -index);
		std::vector<T> &vec = index >= 0 ? positive : negative;
		if (vec.size() <= realIdx) vec.resize(realIdx + 1);
		return vec[realIdx];
	}

private:
	std::vector<T> positive; // >= 0
	std::vector<T> negative; // < 0
};