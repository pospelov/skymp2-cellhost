#pragma once
#include <functional>
#include <memory>
#include <string>
#include <cassert>

#include "NetInterface.h"

// Emits CONNECT once
// Doesn't emit DISCONNECT, trying to reconnect instead
// Emits all MESSAGE's
class AutorecClient : public IClient {
public:
	using ClearClientFactory = std::function<IClient *()>;

	AutorecClient(ClearClientFactory factory) : fac(factory) {
	}

	~AutorecClient() override = default;

	void connect(const char *ipAddress_, unsigned short port_) override {
		if (ipAddress.empty() == false) {
			throw std::runtime_error("already connected");
		}
		ipAddress = ipAddress_;
		port = port_;
		startReconnect();
	}

	bool receive(NetEvent &event) noexcept override {
		NetEvent ne;
		if (client->receive(ne)) {
			switch (ne.type) {
			case NetEvent::CONNECT:
				if (!connectedOnce) {
					connectedOnce = true;
					event = ne;
					return true;
				}
				break;
			case NetEvent::DISCONNECT:
			case NetEvent::CONNECTION_ATTEMPT_FAILED:
				startReconnect();
				break;
			case NetEvent::MESSAGE:
				event = ne;
				return true;
			default:
				break;
			}
		}
		return false;
	}

	void send(const RawMessage &message, Reliability reliability) noexcept override {
		assert(client); // did you forget to call connect?
		client->send(message, reliability);
	}

	int getAvgPing() const noexcept override {
		return client->getAvgPing();
	}

private:
	void startReconnect() {
		client.reset(fac());
		assert(client); // your factory function returned nullptr
		client->connect(ipAddress.c_str(), port);
	}

	const ClearClientFactory fac;
	std::unique_ptr<IClient> client;
	std::string ipAddress;
	unsigned short port = 0;
	bool connectedOnce = false;
};