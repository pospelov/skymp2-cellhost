#pragma once
#include <functional>
#include <array>

class IStreamer {
public:
	using formid = unsigned;
	static_assert(sizeof(formid) == 4);

	using Point = std::array<int, 3>; // X, Y, cell/world id
	static_assert(sizeof(Point) == 4 * 3);

	virtual void moveTo(formid key, const Point &point) noexcept = 0;
	virtual void erase(formid key) noexcept = 0;
	virtual void forEachNeighbour(formid key, std::function<void(formid)> callback) const noexcept = 0;

	virtual ~IStreamer() = default;
};