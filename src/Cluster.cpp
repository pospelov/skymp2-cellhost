#include <vector>
#include <atomic>
#include <thread>
#include <memory>
#include <string>
#include <chrono>
#include <array>
#include <unordered_map>
#include <cassert>
#include <list>
#include <functional>
#include <cstring> // memcpy
#include <boost/lockfree/queue.hpp>

#include "Cluster.h"

class UserInfo {
public:
	UserInfo() = default;
	UserInfo(UserID id_, unsigned short serverId_) : userId(id_), serverId(serverId_) {
	}

	using Key = int;

	Key toKey() const noexcept {
		return *(Key *)this;
	}

	bool isEmpty() const noexcept {
		return userId == g_invalidUserId;
	}

	UserID userId = g_invalidUserId;
	unsigned short serverId = -1;
};

static_assert(sizeof(UserInfo) == 4);
static_assert(sizeof(UserInfo::Key) == 4);

class IdMapping {
public:
	UserID allocateId(UserInfo info) {
		const UserID res = nextId++;
		while (infoById[nextId].isEmpty() == false) {
			++nextId;
			assert(nextId != g_invalidUserId);
		}
		infoById[res] = info;
		idByInfo[info.toKey()] = res;
		return res;
	}

	void freeId(UserID id) {
		const auto info = infoById[id];
		infoById[id] = {};
		idByInfo.erase(info.toKey());
		if (id < nextId) nextId = id;
	}

	UserID find(UserInfo info) {
		try {
			return idByInfo.at(info.toKey());
		}
		catch (...) {
			return g_invalidUserId;
		}
	}

	UserInfo find(UserID id) {
		if ((long long)id >= std::size(infoById) || (long long)id < 0 /* what if we change UserID type to signed and pass -1? */) {
			return {};
		}
		return infoById[id];
	}

private:
	std::array<UserInfo, 65536> infoById;
	std::unordered_map<UserInfo::Key, UserID> idByInfo;
	UserID nextId = 0;
};

template <class T> using LockfreeQueue = boost::lockfree::queue<T, boost::lockfree::capacity<65534>>;

class Worker {
public:
	using Task = std::function<void(IServer *)>;
	using Pair = std::pair<UserID, NetEvent>;

	Worker(IServer *server_) : server(server_) {
		sendThread = new std::thread([this] {
			UserID id;
			NetEvent ne;
			while (1) {
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				Task *fPtr = nullptr;
				while (tasksQ.pop(fPtr)) {
					fPtr->operator()(server);
					delete fPtr;
				}
				while (server->receiveFrom(id, ne)) {
					receiveQ.push(new Pair(id, ne));
				}
			}
		});
	}

	~Worker() {
		sendThread->join();
		delete sendThread;
	}

	void addTask(Task f) {
		tasksQ.push(new Task(f));
	}

	bool receiveFrom(std::function<void(Pair &)> callback) {
		Pair *p;
		if (!receiveQ.pop(p)) return false;
		callback(*p);
		delete p;
		return true;
	}


private:
	std::thread *sendThread = nullptr;
	IServer *const server;
	LockfreeQueue<Task *> tasksQ;
	LockfreeQueue<Pair *> receiveQ;

	Worker(const Worker &) = delete;
	Worker &operator=(const Worker &) = delete;
};

struct Cluster::Impl {
	IServer *const server;
	std::unique_ptr<Worker> worker;

	void open() {
		worker.reset(new Worker(server));
	}

	bool receiveFrom(UserID &user, NetEvent &event) {
		assert(worker);
		return worker->receiveFrom([&](Worker::Pair &p) {
			user = p.first;
			event = p.second;
		});
	}

	void sendTo(UserID target, const void *message, size_t size, Reliability reliability) {
		std::vector<char> data;
		data.resize(size);
		memcpy(&data[0], message, size);
		worker->addTask([=](IServer *server) {
			server->sendTo(target, &data[0], size, reliability);
		});
	}
};

void Cluster::open(unsigned short) {
	pImpl->open();
}

bool Cluster::receiveFrom(UserID &userId, NetEvent &event) noexcept {
	return pImpl->receiveFrom(userId, event);
}

void Cluster::sendTo(UserID target, const void *message, size_t size, Reliability reliability) noexcept {
	pImpl->sendTo(target, message, size, reliability);
}

Cluster::Cluster(IServer *server) : pImpl(new Impl{ server }) {
}

Cluster::~Cluster() {
	delete pImpl;
}