#pragma once
#include "NetInterface.h"
#include <vector>

class Cluster : public IServer {
public:
	void open(unsigned short _unused_ = -1) override;
	bool receiveFrom(UserID &userId, NetEvent &event) noexcept override;
	void sendTo(UserID target, const void *message, size_t size, Reliability reliability) noexcept override;

	Cluster(IServer *servers);
	~Cluster() override;
private:
	struct Impl;
	Impl *const pImpl;

	Cluster(const Cluster &) = delete;
	Cluster &operator=(const Cluster &) = delete;
};